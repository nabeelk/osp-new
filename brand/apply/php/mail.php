<?php
  
if ('POST' === $_SERVER['REQUEST_METHOD']) {
    $name = "";
    $email = "";
    $yt = "";
    $ig = "";
	$snapchat = "";
	$facebook = "";
	$phone = "";
	$email_title = "New Application - OSP Group";
    $email_body = "<div>";
      
    if(isset($_POST['name'])) {
        $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
        $email_body .= "<div>
                           <label><b>Visitor Name:</b></label>&nbsp;<span>".$name."</span>
                        </div>";
    }
 
    if(isset($_POST['email'])) {
        $email = str_replace(array("\r", "\n", "%0a", "%0d"), '', $_POST['email']);
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        $email_body .= "<div>
                           <label><b>Visitor Email:</b></label>&nbsp;<span>".$email."</span>
                        </div>";
    }
      
    if(isset($_POST['yt'])) {
        $yt = filter_var($_POST['yt'], FILTER_SANITIZE_STRING);
        $email_body .= "<div>
                           <label><b>YouTube Channel:</b></label>&nbsp;<span>".$yt."</span>
                        </div>";
    }
      
    if(isset($_POST['ig'])) {
        $ig = filter_var($_POST['ig'], FILTER_SANITIZE_STRING);
        $email_body .= "<div>
                           <label><b>Instagram:</b></label>&nbsp;<span>".$ig."</span>
                        </div>";
    }
      
    if(isset($_POST['snapchat'])) {
        $snapchat = htmlspecialchars($_POST['snapchat']);
        $email_body .= "<div>
                           <label><b>Snapchat:</b></label>
                           <div>".$snapchat."</div>
                        </div>";
    }
	    if(isset($_POST['facebook'])) {
        $facebook = htmlspecialchars($_POST['facebook']);
        $email_body .= "<div>
                           <label><b>Facebook:</b></label>
                           <div>".$facebook."</div>
                        </div>";
    }
	    if(isset($_POST['phone'])) {
        $phone = htmlspecialchars($_POST['phone']);
        $email_body .= "<div>
                           <label><b>Phone No:</b></label>
                           <div>".$phone."</div>
                        </div>";
    }
      
    $recipient = "fahad@osp.ae";
    $email_body .= "</div>";
    $headers  = 'MIME-Version: 1.0' . "\r\n"
    .'Content-type: text/html; charset=utf-8' . "\r\n"
    .'From: ' . $email . "\r\n";
      
    if(mail($recipient, $email_title, $email_body, $headers)) {
        $output['status'] = 'SUCCESS'
        $output['message'] = 'Thank you for contacting us, You will get a reply within 24 hours.';
    } else {
        $output['status'] = 'ERR'
        $output['message'] = 'Please Try again';
    }
      
} else {
    $output['status'] = 'ERR'
    $output['message'] = 'Please Try again';
}
?>