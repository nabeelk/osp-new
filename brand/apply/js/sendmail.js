
$(".submit").on('click',(function(e){
	e.preventDefault();
    $.confirm({
		type: 'blue',
		content: function () {
			var self = this;
			return $.ajax({
				url: "../apply/php/mail.php",
				type: "POST",
				data:  new FormData($('#myform')[0]),
				contentType: false,
				cache: false,
				processData:false,
			}).done(function (data) {
						  //alert(data);
	           var objData = jQuery.parseJSON(data);
                    
                    if (objData.status === 'ERR'){
				   // alert('err');
					self.setTitle('<span class="red bold">'+objData.status+'</span>');
					self.setContent(objData.message);
                    self.buttons.close.hide();
					self.buttons.cancel.setText("Ok");
				}
				else if (objData.status === 'SUCCESS') {
				   // alert('suc');
					 self.setTitle('<span style="color:#060">Status</span>');
						  self.setContent('<span style="color:#060">'+objData.message+'</span>');
						  //$('#loader-icon').hide();
						 //to reset captcha and form after success	
						 $("#frmQuick")[0].reset();
						 //grecaptcha.reset();
				}
						 
				  
					  }).fail(function(){
						  self.setContent('Something went wrong.');
					  });
				  },
				  buttons: {
					  close: {
						btnClass: 'btn-orange',
						action: function () {
							//grecaptcha.reset();
							setTimeout(function(){// wait for 1 secs(1)
										  location.reload(); // then reload the page.(2)
										  }, 100);
							
						}
					  },
                      cancel: function(){
				//$('#myModalCat').modal('toggle');
			}
				  }
      
                                
	});
		

}));
      